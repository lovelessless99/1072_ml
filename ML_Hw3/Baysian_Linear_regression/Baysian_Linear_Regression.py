from random_data_generator.random_generator import polynomial_generator
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
if __name__ == "__main__":
    a = 1
    b = 100
    n = 4
    w = np.array([1, 2, 3, 4])

    mean = np.array([0]*n)
    cov = (1/b) * np.identity(n)
    times10_cov_inv, mean_10 = None, None
    times50_cov_inv, mean_50 = None, None

    x_all_points = []
    y_all_points = []
    variance_all = []

    for i in range(1000):
        x_point = np.random.uniform(low=-1, high=1, size=1)
        x = np.vstack([x_point ** i for i in range(n)]).T
        y = polynomial_generator(x, w, size=1)

        print("Add data point (%s, %s): \n" % (x_point[0], y[0]))
        x_all_points.append(x_point[0])
        y_all_points.append(y[0])

        precision_matrix = np.linalg.inv(cov)
        cov = 1/a*np.dot(x.T, x) + precision_matrix
        mean = np.dot(np.linalg.inv(cov),  1/a*np.dot(x.T, y)+np.dot(precision_matrix, mean))

        print("Postirior mean: ")
        print(mean)
        print("Posterior variance: ")
        print(cov)

        if i is 9:
            times10_cov_inv, mean_10 = np.linalg.inv(cov), mean
        if i is 49:
            times50_cov_inv, mean_50 = np.linalg.inv(cov), mean

        prediction = (np.dot(x, mean)[0], 1/a + np.dot(x, np.dot(np.linalg.inv(cov), x.T))[0][0])
        variance_all.append(prediction[1])

        print("Predictive distribution ~ N(%s, %s)\n\n" % prediction)

    x_point = np.linspace(-2, 2, 1000)
    x = np.vstack([x_point ** i for i in range(n)]).T

    # Ground Truth
    sns.set()
    plt.subplot(221)
    plt.title("Ground truth")
    plt.axis([-2, 2, -20, 20])
    plt.plot(x_point, np.dot(x, w), "-k")
    plt.plot(x_point, [k + a for k in np.dot(x, w)], "-r")
    plt.plot(x_point, [k - a for k in np.dot(x, w)], "-r")


    # Predict Result
    sns.set()
    plt.subplot(222)
    plt.title("Predict Result")
    plt.axis([-2, 2, -20, 20])
    plt.plot(x_point, np.dot(x, mean), "-k")
    plt.plot(x_all_points[:200], y_all_points[:200], "o")
    plt.plot(x_point, [y + np.sqrt(variance_all[-1]) for y in np.dot(x, mean)], "-r")  # 要改
    plt.plot(x_point, [y - np.sqrt(variance_all[-1]) for y in np.dot(x, mean)], "-r")  # 要改

    # After 10 incomes
    sns.set()
    plt.subplot(223)
    plt.title("After 10 incomes")
    plt.axis([-2, 2, -20, 20])
    plt.plot(x_point, np.dot(x, mean_10), "-k")
    plt.plot(x_all_points[:10], y_all_points[:10], "o")

    variance_times10 = [1/a + np.dot(data, np.dot(times10_cov_inv, data.T))for data in x]
    plt.plot(x_point, [y + np.sqrt(var) for y, var in zip(np.dot(x, mean_10), variance_times10)], "-r")
    plt.plot(x_point, [y - np.sqrt(var) for y, var in zip(np.dot(x, mean_10), variance_times10)], "-r")

    # After 50 incomes
    sns.set()
    plt.subplot(224)
    plt.title("After 50 incomes")
    plt.axis([-2, 2, -20, 20])
    plt.plot(x_point, np.dot(x, mean_50), "-k")
    plt.plot(x_all_points[:50], y_all_points[:50], "o")

    variance_times50 = [1 / a + np.dot(data, np.dot(times50_cov_inv, data.T)) for data in x]
    plt.plot(x_point, [y + np.sqrt(var) for y, var in zip(np.dot(x, mean_50), variance_times50)], "-r")
    plt.plot(x_point, [y - np.sqrt(var) for y, var in zip(np.dot(x, mean_50), variance_times50)], "-r")

    plt.show()
