from random_data_generator.random_generator import box_muller_sampling


def update(existingAggregate, newValue):
    (count, mean, M2) = existingAggregate
    count += 1
    delta = newValue - mean
    mean += delta / count
    delta2 = newValue - mean
    M2 += delta * delta2

    return count, mean, M2


# retrieve the mean, variance and sample variance from an aggregate
def finalize(existingAggregate):
    (count, mean, M2) = existingAggregate
    (mean, variance, sampleVariance) = (mean, M2 / count, M2 / (count - 1))
    if count < 2:
        return float('nan')
    else:
        return mean, variance, sampleVariance


if __name__ == "__main__":
    import numpy as np
    mean = 3
    sigma = np.sqrt(5)
    print("Data point source function: N(%.f, %.f)\n" % (mean, sigma))
    Gaussian_sampling = box_muller_sampling(mu=mean, sigma=sigma, size=20000)
    init = (1, mean, 0)

    for new_value in Gaussian_sampling:
        print("Add data point: %s" % new_value)
        init = update(init, new_value)
        result = finalize(init)
        print("Mean = %s Variance = %s" % (result[0], result[1]))

