import numpy as np
import struct
import gzip
import os


def load(dataSrc, labelsSrc, cimg):
    data = loadData(dataSrc, cimg)
    labels = loadLabels(labelsSrc, cimg)
    return data, labels
    # return np.hstack((data, labels))


def loadData(gzfname, cimg):

    with gzip.open(gzfname) as gz:
        n = struct.unpack('I', gz.read(4))

        # 判斷是否為 MNIST 資料集
        if n[0] != 0x3080000:
            raise Exception('Invalid file: unexpected magic number.')

        # 計算資料筆數
        n = struct.unpack('>I', gz.read(4))[0]

        if n != cimg:
            raise Exception('Invalid file: expected {0} entries.'.format(cimg))

        crow = struct.unpack('>I', gz.read(4))[0]
        ccol = struct.unpack('>I', gz.read(4))[0]

        if crow != 28 or ccol != 28:
            raise Exception('Invalid file: expected 28 rows/cols per image.')

        # 讀取資料
        res = np.fromstring(gz.read(cimg * crow * ccol), dtype=np.uint8)

    return res.reshape((cimg, crow * ccol))


def loadLabels(gzfname, cimg):

    with gzip.open(gzfname) as gz:
        n = struct.unpack('I', gz.read(4))

        # 判斷是否為 MNIST 資料集
        if n[0] != 0x1080000:
            raise Exception('Invalid file: unexpected magic number.')

        # 計算資料筆數
        n = struct.unpack('>I', gz.read(4))

        if n[0] != cimg:
            raise Exception('Invalid file: expected {0} rows.'.format(cimg))

        # 讀取資料
        res = np.fromstring(gz.read(cimg), dtype=np.uint8)

    return res.reshape((cimg, 1))
