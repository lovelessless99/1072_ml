from MNIST.load_data import load
import numpy as np

SEED = 483982
PRNG = np.random.RandomState(SEED)

## NUMBER OF CLUSTERS ##
NUMBER_OF_CLUSTERS = 10


####

def em(observations, cont_tol, iterations):
    [data_num, feature_num] = observations.shape
    iteration = 1
    delta_change = 9999

    # Init Model
    pi = np.random.rand(NUMBER_OF_CLUSTERS)
    mu = np.random.rand(NUMBER_OF_CLUSTERS, feature_num)
    r = np.zeros([data_num, NUMBER_OF_CLUSTERS])  # soft assignment
    weight = np.zeros(NUMBER_OF_CLUSTERS)

    # Main loop
    while iteration <= iterations:

        # E step

        for data in range(0, data_num):
            observation = observations[data]
            weight = pi
            result = np.prod(np.multiply(np.power(mu, observation),
                                         np.power(np.ones(feature_num) - np.array(mu),
                                                  np.ones(feature_num) - np.array(observation)))
                             , axis=1)
            weight = np.multiply(weight, result)

            r[data, :] = weight / sum(weight)

        # M step
        nk = np.sum(r.T, axis=1)

        new_mu = np.zeros([NUMBER_OF_CLUSTERS, feature_num])
        for kk in range(0, NUMBER_OF_CLUSTERS):
            mean = np.zeros(feature_num)
            for ii in range(0, data_num):
                mean += r[ii, kk] * observations[ii]
            new_mu[kk] = mean / nk[kk]
        # print(np.shape(observations))
        # print(np.shape(r))
        pi = nk / sum(nk)

        delta_change = sum(sum(abs(new_mu - mu)))
        if delta_change < cont_tol:
            break
        else:
            mu = new_mu
            iteration += 1

        print("finish %s iteration" % iteration)

    return [mu, pi, iteration]


if __name__ == "__main__":
    import time
    start = time.time()
    X_train, Y_train = load("../MNIST/train-images.gz", "../MNIST/train-labels.gz", 60000)
    X_test, Y_test = load("../MNIST/test-images.gz", "../MNIST/test-labels.gz", 10000)

    regularize = np.where(X_train > 0, 1, 0).T
    pmf = np.array([np.bincount(i) / 60000 for i in regularize])  # 要估計的

    cont_tol = 1e-5
    max_iter = 1000
    [mu_est, pi_est, iterations] = em(regularize.T, cont_tol, max_iter)
    print([mu_est, pi_est, iterations])
    end = time.time()
    print("Total time = %s" % (end - start))
