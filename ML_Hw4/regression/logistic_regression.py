from random_data_generator.random_generator import box_muller_sampling
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd
import seaborn as sns


def logistic(x, w):
    return 1 / (1 + np.exp(-np.dot(x, w)))


def plot_all_graph(X, Y, title, order):
    sns.set()
    colors = ['red', 'blue']
    plt.subplot(1, 3, order)
    plt.scatter(X["x1"], X["x2"], c=X[Y], cmap=matplotlib.colors.ListedColormap(colors))
    plt.title(title)


def gradient_descent(mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n):
    w = np.random.rand(3)
    for _ in range(10000):
        X, Y = generate_data(mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n)
        gradient = np.dot(X.T, logistic(X, w) - Y)
        w = w - gradient
    return w


def netwon_method(mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n):
    w = np.random.rand(3)
    for _ in range(10000):
        X, Y = generate_data(mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n)
        gradient = np.dot(X.T, Y - logistic(X, w))
        diag_element = (np.exp(-np.dot(X, w))) / (1 + np.exp(-np.dot(X, w)))**2
        inv_hessian = np.linalg.inv(np.dot(X.T, np.dot(np.diag(diag_element), X)))
        w = w + 0.01 * np.dot(inv_hessian, gradient)
    return w


def calculate_confusion_mat(df, predict):
    Confusion_Matrix = pd.DataFrame(columns=["Predict cluster 1", "Predict cluster 2"], index=["Is cluster 1",
                                                                                               "Is cluster 2"]).fillna(0)

    for i, j in zip(df["labels"], df[predict]):
        if i == j == 0:
            Confusion_Matrix.loc['Is cluster 1', 'Predict cluster 1'] += 1
        elif i == j == 1:
            Confusion_Matrix.loc['Is cluster 2', 'Predict cluster 2'] += 1
        elif i == 0 and j == 1:
            Confusion_Matrix.loc['Is cluster 1', 'Predict cluster 2'] += 1
        elif i == 1 and j == 0:
            Confusion_Matrix.loc['Is cluster 2', 'Predict cluster 1'] += 1

    sensitivity = Confusion_Matrix.loc['Is cluster 1', 'Predict cluster 1'] / \
                  ((Confusion_Matrix.loc['Is cluster 1', 'Predict cluster 1'] +\
                  Confusion_Matrix.loc['Is cluster 1', 'Predict cluster 2']))

    specificity = Confusion_Matrix.loc['Is cluster 2', 'Predict cluster 2'] / \
                  ((Confusion_Matrix.loc['Is cluster 2', 'Predict cluster 1'] +\
                  Confusion_Matrix.loc['Is cluster 2', 'Predict cluster 2']))
    print("Confusion Matrix:")
    print(Confusion_Matrix)
    print("Sensitivity (Successfully predict cluster 1): %s" % sensitivity)
    print("Specificity (Successfully predict cluster 2): %s" % specificity)


def generate_data(mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n):
    vx1, vy1, vx2, vy2 = np.sqrt([vx1, vy1, vx2, vy2])

    # data 1
    x1 = np.array(box_muller_sampling(mu=mx1, sigma=vx1, size=n))
    y1 = np.array(box_muller_sampling(mu=my1, sigma=vy1, size=n))

    # data 2
    x2 = np.array(box_muller_sampling(mu=mx2, sigma=vx2, size=n))
    y2 = np.array(box_muller_sampling(mu=my2, sigma=vy2, size=n))

    X1 = np.vstack((np.repeat(1, n), x1, y1)).T
    X2 = np.vstack((np.repeat(1, n), x2, y2)).T
    X = np.concatenate((X1, X2))
    Y = np.concatenate((np.repeat(0, n), np.repeat(1, n)))

    return X, Y


if __name__ == "__main__":
    # mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n = 1, 2, 1, 2, 10, 2, 10, 2, 50
    mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n = 1, 2, 1, 2, 3, 4, 3, 4, 50
    X, Y = generate_data(mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n)

    w_gradient = gradient_descent(mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n)
    gradient_predict = [0 if i < 0 else 1 for i in np.dot(X, w_gradient)]

    w_netwon = netwon_method(mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n)
    netwon_predict = [0 if i < 0 else 1 for i in np.dot(X, w_netwon)]

    df = pd.DataFrame(np.delete(X, 0, 1), columns=["x1", "x2"])
    df["labels"] = pd.Series(Y)
    df["gradient_predicts"] = pd.Series(gradient_predict)
    df["Netwon_predicts"] = pd.Series(netwon_predict)

    print("Gradient descent")
    print("w:\n %s" % w_gradient)
    calculate_confusion_mat(df, "gradient_predicts")
    print("-------------------------------------------------------")

    print("Newton's method:")
    print("w:\n %s" % w_netwon)
    calculate_confusion_mat(df, "Netwon_predicts")

    plot_all_graph(df, "labels", "Ground truth", 1)
    plot_all_graph(df, "gradient_predicts",  "gradient descent", 2)
    plot_all_graph(df, "Netwon_predicts", "Netwon descent", 3)


    plt.show()
