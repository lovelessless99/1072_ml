import numpy as np


def box_muller_sampling(mu=0, sigma=1, size=1):
    u = np.random.uniform(size=size)
    v = np.random.uniform(size=size)
    z = np.sqrt(-2 * np.log(u)) * np.cos(2 * np.pi * v)
    return mu + z * sigma


def polynomial_generator(x, weight, size=1, sigma=1):
    # x = np.random.uniform(low=-1, high=1, size=size)
    e = box_muller_sampling(mu=0, sigma=sigma, size=size)
    return np.dot(x, weight) + e


if __name__ == "__main__":
    size = 30
    x = np.random.uniform(low=-1, high=1, size=size)
    weight = np.random.rand(size)
    print(polynomial_generator(x=x, weight=weight, size=size, sigma=10))
