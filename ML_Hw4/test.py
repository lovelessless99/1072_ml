import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from random_data_generator.random_generator import box_muller_sampling

def normalize(train_x):
    mu = train_x.mean()
    sigma = train_x.std()
    return (train_x-mu)/sigma


def logistic_regression(train_x, train_y, epoch, eta):
    theta = np.random.rand(3)
    # X = np.hstack([np.ones([train_x.shape[0], 1]), train_x])
    f = lambda x: 1 / (1 + np.exp(-np.dot(x, theta)))

    for _ in range(epoch):
        theta = theta - eta * np.dot(f(X) - train_y, X)
    return theta


def generate_data(mx1, vx1, my1, vy1, mx2, vx2, my2, vy2, n):
    vx1, vy1, vx2, vy2 = np.sqrt([vx1, vy1, vx2, vy2])

    # data 1
    x1 = np.array(box_muller_sampling(mu=mx1, sigma=vx1, size=n))
    y1 = np.array(box_muller_sampling(mu=my1, sigma=vy1, size=n))

    # data 2
    x2 = np.array(box_muller_sampling(mu=mx2, sigma=vx2, size=n))
    y2 = np.array(box_muller_sampling(mu=my2, sigma=vy2, size=n))

    # x = np.concatenate((x1, y1, np.repeat(1, n)))

    X1 = np.vstack((np.repeat(1, n), x1, y1)).T
    X2 = np.vstack((np.repeat(1, n), x2, y2)).T
    X = np.concatenate((X1, X2))
    Y = np.concatenate((np.repeat(0, n), np.repeat(1, n)))
    # X = np.vstack([x ** i for i in range(3)]).T
    # x = np.concatenate((x1, x2))
    # X = np.vstack([x ** i for i in range(3)]).T


    return X, Y


if __name__ == '__main__':
    A = [[3, 2, 3, 4, 5], [3, 2, 3, 4, 5]]
    b = [2, 3, 4, 5, 6]
    # print(np.prod(np.multiply(np.power(A, b),np.power(np.ones(5)-A, np.ones(5)-b)), axis=1))
    print(np.sum(A, axis=1))