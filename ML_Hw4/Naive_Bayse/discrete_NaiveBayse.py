from MNIST.load_data import load
import pandas as pd
import numpy as np


def discrete_mode(X_train, Y_train, X_test, Y_test):
    X_train1, Y_train1 = pd.DataFrame(X_train), pd.DataFrame(Y_train, columns=['output'])
    train_data = pd.concat([X_train1, Y_train1], axis=1, join='inner')
    class_num = len(np.unique(Y_train))

    sector = train_data.groupby('output')
    all_pmf = []

    for label in range(class_num):
        pmf = []
        for i in sector.get_group(label).iloc[:, :-1]:
            count = np.repeat(0, 32)
            class_group = sector.get_group(label)[i]
            for h in class_group:
                count[h] += 1

            # min_value = np.min(count[np.nonzero(count)])
            count = count / class_group.shape
            # count[i == 0] = 0.001
            for index, i in enumerate(count):
                if i == 0:
                    count[index] = 0.00001 # 數值問題

            pmf.append(count)

        print("finish class %d pmf, shape = %s" % (label, np.shape(pmf)))
        all_pmf.append(pmf)

    priority = train_data.iloc[:, -1].value_counts().values / len(train_data)

    all_posterior = []
    for count, image in enumerate(X_test):
        image_all_class_posterior = []
        for label in range(class_num):
            posterior = np.log(priority[label])
            for index, pixel in enumerate(image):
                posterior += np.log(all_pmf[label][index][pixel])

            image_all_class_posterior.append(posterior)

        image_all_class_posterior = [x/sum(image_all_class_posterior) for x in image_all_class_posterior]
        print("Postirior (in log scale):")
        for index, i in enumerate(image_all_class_posterior):
            print("%s: %s" % (index, i))

        print("Prediction: %s, Ans: %s\n\n"%(Y_test[count], np.argmin(image_all_class_posterior)))

        all_posterior.append(image_all_class_posterior)

    print("Imagination of numbers in Bayesian classifier:")
    for index, image in enumerate(all_pmf):
        print("%d :" % index)
        str = ""
        for count, pixel in enumerate(image):
            if count % 28 == 0 and count != 0:
                print(str)
                str = ""
            else:
                if np.argmax(pixel) == 0:
                    str += " 0"
                else:
                    str += " 1"

    output = [np.argmin(i) for i in all_posterior]
    print("Error rate: %s" % (1 - np.mean(Y_test == output)))