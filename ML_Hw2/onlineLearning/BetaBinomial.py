from scipy.special import comb, perm
import numpy as np

if __name__ == "__main__":
    data = np.loadtxt("../data/testfile.txt", dtype='str')
    alpha = 0
    beta = 0


    for index, case in enumerate(data):
        binominal = np.bincount([int(i) for i in list(case)])
        print("case %d: %s" % (index+1, case))
        times = len(list(case))
        success = binominal[1] / times
        fail = binominal[0] / times
        print("Likelilood: %.10f" % (comb(times, binominal[1]) * (success**binominal[1]) * (fail**binominal[0]) ))
        print("Beta prior:      a = %d    b = %d" % (alpha, beta))
        alpha, beta = alpha + binominal[1], beta + binominal[0]
        print("Beta posterior:  a = %d    b = %d\n\n" % (alpha, beta))
