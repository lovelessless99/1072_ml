from Naive_Bayse.continuous_naive_bayes import GaussianNB
from Naive_Bayse.discrete_NaiveBayse import discrete_mode
from MNIST.load_data import load
import numpy as np

if __name__ == "__main__":
    X_train, Y_train = load("MNIST/train-images.gz", "MNIST/train-labels.gz", 60000)
    X_test, Y_test = load("MNIST/test-images.gz", "MNIST/test-labels.gz", 10000)
    X_train, X_test = X_train // 8, X_test // 8
    Y_train, Y_test = Y_train.flatten(), Y_test.flatten()

    mode = input("input discrete(0) or continuous(1):")

    if mode is "0":
        discrete_mode(X_train, Y_train, X_test, Y_test)

    else:
        classifier = GaussianNB().fit(X_train, Y_train)
        output = classifier.predict(X_test, Y_test)
        classifier.print_number()
        print("Error rate: %s" % 1 - np.mean(Y_test == output))