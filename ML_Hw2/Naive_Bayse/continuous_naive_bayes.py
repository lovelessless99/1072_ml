from sklearn.model_selection import train_test_split
from scipy.stats import norm
from MNIST.load_data import load
import itertools
import pandas as pd
import numpy as np


def gaussian_pdf(x, mean, sigma):
    if sigma != 0.0:
        # print("")
        # print( 1/(sigma*np.sqrt(2*np.pi)) * np.exp(-(x-mean)**2/(2*sigma**2)))
        return 1/(sigma*np.sqrt(2*np.pi)) * np.exp(-(x-mean)**2/(2*sigma**2))



class GaussianNB:
    def __init__(self):
        self.mu_list = []
        self.std_list = []
        self.pi_list = []

    def fit(self, X_train, Y_train):
        X_train, Y_train = pd.DataFrame(X_train), pd.DataFrame(Y_train, columns=['output'])
        train_data = pd.concat([X_train, Y_train], axis=1, join='inner')
        class_num = len(np.unique(Y_train))
        self.mu_list = np.split(train_data.groupby('output').mean().values, class_num)
        self.std_list = np.split(train_data.groupby('output').std().values, class_num)
        self.pi_list = train_data.iloc[:, -1].value_counts().values / len(train_data)

        self.mu_list = list(itertools.chain(*self.mu_list))
        self.std_list = list(itertools.chain(*self.std_list))

        return self

    def predict(self, X_validation, Y_validation):
        output = []
        classes = len(self.mu_list)
        all_posterior = []

        for count, image in enumerate(X_validation):
            image_all_class_posterior = []
            for label in range(classes):
                posterior = np.log(self.pi_list[label])
                for index_row, val in enumerate(image.tolist()):
                    if self.std_list[label][index_row] != 0.0:

                        #a = 1/(self.std_list[p][index_row]*np.sqrt(2*np.pi)) * np.exp(-(val-self.mu_list[p][index_row])**2/(2*self.std_list[p][index_row]**2))
                        a = norm.pdf(x=val, loc=self.mu_list[label][index_row], scale=self.std_list[label][index_row])
                        if a !=0.0:
                            posterior += np.log(a)
                        # score += np.log(norm.pdf(x=val, loc=self.mu_list[p][index_row], scale=self.std_list[p][index_row]))

                image_all_class_posterior.append(posterior)
            image_all_class_posterior = [x / sum(image_all_class_posterior) for x in image_all_class_posterior]
            # scores_list.append(posterior)
            print("Postirior (in log scale):")
            for index, i in enumerate(image_all_class_posterior):
                print("%s: %s" % (index, i))

            print("Prediction: %s, Ans: %s\n\n" % (Y_validation[count], np.argmin(image_all_class_posterior)))
            all_posterior.append(image_all_class_posterior)
            # print(scores_list)
            output.append(np.argmax(image_all_class_posterior))

        return output

    def print_number(self):
        print("Imagination of numbers in Bayesian classifier:")
        for index, image in enumerate(self.mu_list):
            print("%d :" % index)
            str = ""
            for count, pixel in enumerate(image):
                if count % 28 == 0 and count != 0:
                    print(str)
                    str = ""

                else:
                    if pixel < 10:
                        str += " 0"
                    else:
                        str += " 1"


if __name__ == "__main__":
    X_train, Y_train = load("../MNIST/train-images.gz", "../MNIST/train-labels.gz", 60000)

    X_test, Y_test = load("../MNIST/test-images.gz", "../MNIST/test-labels.gz", 10000)

    X_train, X_test = X_train // 8, X_test // 8
    Y_train, Y_test = Y_train.flatten(), Y_test.flatten()
    # output = GaussianNB().fit(X_train, Y_train).predict(X_test, Y_test)
    # print("Error rate: %s" % 1 - np.mean(Y_test == output))

    GaussianNB().fit(X_train, Y_train).print_number()

