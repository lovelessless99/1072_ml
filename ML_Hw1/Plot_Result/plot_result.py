import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


class PlotAllResult:
    def __init__(self, x, y, n, lam, theta_netwon, theta_rlse, error_newton, error_rLSE):
        self.x = x
        self.y = y
        self.n = n
        self.lam = lam

        self.theta_netwon = theta_netwon
        self.theta_rlse = theta_rlse
        self.error_newton = error_newton
        self.error_rLSE = error_rLSE

    def plot_result(self, theta, start, linecolor, text_xposition):
        it_n = iter(self.n)
        it_lam = iter(self.lam)
        for count, i in enumerate(theta):
            x_data = np.linspace(-5, 5, 100)
            y_data = np.vstack([x_data ** power for power in range(len(i))]).T.dot(i)

            sns.set()
            plt.subplot(2, len(theta), count + start + 1)
            plt.plot(self.x, self.y, "o", label="training data")
            plt.plot(x_data, y_data, linecolor, label="fitting line")
            title = "n = %d, $\lambda = %d$" % (next(it_n), next(it_lam)) if start is 0 else "n = %d" % (next(it_n))
            plt.text(text_xposition, 80, title, size=15)
            plt.legend()

    def plot_all_figure(self):
        self.plot_result(self.theta_rlse, 0, "r", -3)
        self.plot_result(self.theta_netwon, len(self.theta_rlse), "g", -1.2)
        plt.show()
        self.print_all_result()

    def print_all_result(self):
        open('result.txt', 'w').close()
        for count, i in enumerate(zip(self.n, self.lam)):
            Case = "Case %d : n = %d , lambda = %d\n" % (count + 1, i[0], i[1])

            for number, element in enumerate(zip(self.theta_rlse[count], self.theta_netwon[count])):
                if number is 0:
                    newton_formula, rlse_formula = str(element[0]), str(element[1])
                else:
                    newton_formula = "%fX^%d + " % (element[0], number) + newton_formula
                    rlse_formula = "%fX^%d + " % (element[1], number) + rlse_formula

            LSE = "LSE:\nFitting line: %s\nTotal error: %f\n\n" % (newton_formula, self.error_rLSE[count])
            Netwon = "Newton's Method:\nFitting line: %s\nTotal error: %f\n\n\n" % (
                rlse_formula, self.error_newton[count])

            result = Case + LSE + Netwon
            print(result)

            with open("result.txt", 'a') as fw:
                fw.write(result)
