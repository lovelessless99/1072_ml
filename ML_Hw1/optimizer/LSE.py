from optimizer.LU_decompose import LU_inverse

import numpy as np
import functools


def regularize_least_square_error(x, y, n, lam):
    it_lam = iter(lam)
    for _ in n:
        A = np.vstack([x ** i for i in range(_)]).T
        theta = functools.reduce(np.dot, (LU_inverse(np.dot(A.T, A) + np.identity(_) * next(it_lam)), A.T, y))
        error = np.linalg.norm(A.dot(theta) - y)**2
        yield theta,  error
