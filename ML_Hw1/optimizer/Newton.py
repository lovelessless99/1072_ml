from functools import reduce
from optimizer.LU_decompose import LU_inverse
import numpy as np


def newton_method(x, y, n):
    for _ in n:
        A = np.vstack([x ** i for i in range(_)]).T

        diff = 1
        theta = np.random.rand(_)
        error = square_error(A, y, theta)
        while diff > 1e-2:
            inv_hessian = LU_inverse(2 * np.dot(A.T, A))
            gradient = 2 * (reduce(np.dot, (A.T, A, theta)) - np.dot(A.T, y))
            theta = theta - np.dot(inv_hessian, gradient)
            current_error = square_error(A, y, theta)
            diff = error - current_error
            error = current_error

        yield theta, square_error(A, y, theta)


def polynomial(x, theta):
    return np.dot(x, theta)


def square_error(x, y, theta):
    return np.sum((polynomial(x, theta) - y) ** 2)
