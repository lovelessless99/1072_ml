import numpy as np


def LU_decompose(A):
    n = np.shape(A)[0]

    L = np.zeros(np.shape(A))
    D = np.zeros(np.shape(A))

    for k in range(n - 1):
        vector = np.copy(A[:, k])
        vector[k + 1:] = vector[k + 1:] / vector[k]
        vector[0:k + 1] = np.zeros(k + 1)
        L[:, k] = vector
        L[k, k] = 1.0

        for i in range(k + 1, n):
            A[i, :] = A[i, :] - vector[i] * A[k, :]

    L[n - 1, n - 1] = 1.0
    U = np.array(A).astype(float)

    for i in range(n):
        D[i, i] = U[i, i]
        U[i, :] = U[i, :] / U[i, i]

    return L, D, U


def triangular_inverse(**kwargs):

    for key, value in kwargs.items():
        one = np.identity(np.shape(value)[0])
        if key == 'Lower':
            for j in range(0, np.shape(value)[0]):
                for i in range(j + 1, np.shape(value)[0]):
                    one[i, :] = one[j, :] * -value[i, j] + one[i, :]

        elif key == 'Diagonal':
            for i in range(np.shape(value)[1]):
                one[i, i] = 1 / value[i, i]

        elif key == 'Upper':
            for j in range(np.shape(value)[0] - 1, -1, -1):
                for i in range(j - 1, -1, -1):
                    one[i, :] = one[j, :] * -value[i, j] + one[i, :]

        yield one


def LU_inverse(A):
    L, D, U = LU_decompose(np.copy(A))
    L_inv, D_inv, U_inv = triangular_inverse(Lower=L, Diagonal=D, Upper=U)
    return np.dot(U_inv, np.dot(D_inv, L_inv))


if __name__ == '__main__':

    A = np.array([
        [2, -3, 4, 2],
        [4, -5, 10, 5],
        [2, 2, 11, 9],
        [6, -9, 12, 5]
    ])
    L, D, U = LU_decompose(np.copy(A))

    print(LU_inverse(A))
    print(np.linalg.inv(A))
    # print(LU_inverse(A_Pivot))