from optimizer.LSE import regularize_least_square_error as rLSE
from optimizer.Newton import newton_method
from Plot_Result.plot_result import PlotAllResult
from multiprocessing import Pool
import numpy as np
import os


def parallel(x, y, n, lam=None):
    # print("啟動 Process id :{}".format(os.getpid()))
    return tuple(newton_method(x, y, n)) if lam is None else tuple(rLSE(x, y, n, lam))


if __name__ == "__main__":
    x, y = np.loadtxt("data/data.txt", delimiter=',', unpack=True)
    n, lam = np.loadtxt("data/test.txt", delimiter=',', unpack=True, dtype=int)

    with Pool(processes=2) as p:
        netwon = p.apply_async(parallel, args=(x, y, n))
        rlse = p.apply_async(parallel, args=(x, y, n, lam))
        p.close()
        p.join()

        theta_newton, error_newton = zip(*netwon.get())
        theta_rLSE, error_rLSE = zip(*rlse.get())

        PlotAllResult(x, y,  n, lam, theta_newton, theta_rLSE, error_newton, error_rLSE).plot_all_figure()
